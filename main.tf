# configuração do provedor aws
provider "aws" {
    version = "~> 3.0"
    region  = "us-east-1"
}

provider "aws" {
    #usasse o alias para diferenciar um providesr do outro
    alias = "us-east-2"
    version = "~> 3.0"
    region  = "us-east-2"
}

#Provisionando duas 3 instâncias
resource "aws_instance" "dev" {
    count = 3 # Número de instâncias que queremos subir
    ami = var.amis["us-east-1"]
    instance_type = "t2.micro"
    key_name = var.terraform_aws_key
    tags = {
        Name = "dev${count.index}"
    }

    #Vinculando o grupo de segurança criado às instâncias
    vpc_security_group_ids = ["${aws_security_group.acesso_ssh.id}"]
}

resource "aws_instance" "dev4" {
    ami = var.amis["us-east-1"]
    instance_type = "t2.micro"
    key_name= var.terraform_aws_key
    tags = {
        Name = "dev4"
    }

    vpc_security_group_ids = ["${aws_security_group.acesso_ssh.id}"]
    
    #Só será criado esse recurso se o bucket for criado primeiramente
    depends_on= [aws_s3_bucket.dev4]
}

resource "aws_instance" "dev6" {
    provider = aws.us-east-2
    ami = var.amis["us-east-2"]
    instance_type = "t2.micro"
    key_name= var.terraform_aws_key
    tags = {
        Name = "dev6"
    }

    vpc_security_group_ids = ["${aws_security_group.acesso_ssh_us_east_2.id}"]

    depends_on = [aws_dynamodb_table.dynamodb-homologacao]
}

#Criando um bucket s3
resource "aws_s3_bucket" "dev4" {
  bucket = "zgracilabs-dev4"

  #permissionamento do bucket
  acl    = "private"

  tags = {
    Name        = "zgracilabs-dev4"
  }
}

resource "aws_dynamodb_table" "dynamodb-homologacao" {
  provider = aws.us-east-2
  name           = "GameScores"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }
}
