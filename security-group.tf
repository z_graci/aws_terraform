#Criando security group com acesso ssh 
resource "aws_security_group" "acesso_ssh" {
  name        = "acesso_ssh"
  description = "Acesso shh"

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = var.cdirs_blocks
  }
  tags = {
    Name = "ssh"
  }
}


resource "aws_security_group" "acesso_ssh_us_east_2" {
  #define em qual região será criado este recurso
  provider    =  aws.us-east-2 
  name        = "acesso_ssh"
  description = "Acesso shh"

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = var.cdirs_blocks
  }
  tags = {
    Name = "ssh"
  }
}
