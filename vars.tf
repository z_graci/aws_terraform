variable "amis" {
    type = map

    default = {
        "us-east-1" = "ami-0279c3b3186e54acd"
        "us-east-2" = "ami-002068ed284fb165b"
    }
}

variable "cdirs_blocks" {
    type = list
    default = ["170.231.85.219/32"]
}

variable "terraform_aws_key" {
    default = "terraform-aws"
}