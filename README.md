#O que é Terraform?

O terraform é uma ferramenta multiplataforma que gerencia a infraestrutura da sua aplicação;


#Configuração do terraform: utilizar o tfenv para gerenciar a versão que vc quer utilizar do terraform

git clone https://github.com/tfutils/tfenv.git ~/.tfenv
echo 'export PATH="$HOME/.tfenv/bin:$PATH"' >> ~/.bash_profile
echo 'export PATH="$HOME/.tfenv/bin:$PATH"' >> ~/.zshrc
tfenv install
tfenv install 0.14.8
tfenv
tfenv list
tfenv use lates

#Requerimentos:

-> Criar um acesso no IAM

-> Instalar aws cli

-> Configurar aws cli (aws configure -> utilizando as credenciais do IAM)

-> Como criar uma chave local rsa para acessar as máquinas via terraform: 
 
    1) Criando a chave dentro da pasta do projeto terraform
    ssh-keygen -f terraform-aws -t rsa  

    2) Opcional: Movendo a chave privada para o diretório correto
    mv terraform-aws ~/.ssh/

    3) Opcional: Movendo uma cópia da chave pública também para lá
    cp terraform-aws.pub ~/.ssh/  

    4) Importar a chave pública criada dentro doo aws  a partir no menu "EC2-> Pares de chaves;"

#Como remover um recurso já criado?

- Pode apagar o bloco de código do recurso e executar novamente o "terraform plan" e "terraform apply";

- Pode apagar via comando: "terraform destroy -target aws_instance.dev4"

#Curiosidades:

-> terraform show -> mostra uma foto de como está seu ambiente;

-> arquivo terraform.state -> É uma fotografia de como está sua configuração no aws;

-> aws ec2 describe-security-groups -> vizualiza o security group que está presente no aws

-> Para acessar via ssh a máquina deverá utilizar a chave privada rsa que criamos anteriormente:

Ex.: ssh -i ~/.ssh/terraform-aws ubuntu@ec2-35-175-151-70.compute-1.amazonaws.com

-> Organização das pastas: Se os arquivos .tf estiverem no mesmo diretório a comunicação é automática entre os arquivos. 

->  É possível compartilhar o state na Terraform Cloud. Doc: https://www.terraform.io/language/settings/backends/configuration